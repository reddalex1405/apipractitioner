import {Router} from 'express'
const router =  Router();

//Databases
import {connect} from '../database'
import { ObjectID } from 'mongodb'
import bcrypt from 'bcrypt'

const saltRound = 10;

router.get('/', async (req, res)=>{
    const db =  await connect();
    const result = await db.collection('users').find({}).toArray();
    res.json(result);
})

router.post('/', async (req, res) => {

    try{   
        let passEncrypt;
        bcrypt.hash(req.body.password, saltRound, (err, hashedPassword) =>{
            if(!err){
                passEncrypt = hashedPassword;
            }
        })

        const db =  await connect();   
        const user = {
            nombre: req.body.nombre,
            aPaterno: req.body.aPaterno,
            aMaterno: req.body.aMaterno,
            telefono: req.body.telefono,
            direccion: req.body.direccion,
            password: passEncrypt,
            logged: req.body.logged,
            username: req.body.username
        }

        let userFind =  await db.collection('users').findOne(user)

        if(userFind === null){
            const result = await db.collection('users').insertOne(user);
            res.json(result.ops[0])
        }else {
            res.status(400).json({ "error":"Usuario ya existe"})
        }
    } catch(e){
        res.status(400).json({ "error":"Oucrrío un Problema en el guardado, consulte con su administrador"})
    }
    
})

router.get('/getUser/:id', async (req, res) => {
    try{
        const db =  await connect();   
        const { id } = req.params;
        const result = await db.collection('users').findOne({_id: ObjectID(id)})
        
        if (result !== null){
            res.json(result)
        } else {
            res.json({ "error":"Usuario no encontrado"})
        }
    } catch(e) {
        res.status(400).json({ "error":"Oucrrío un Problema, consulte con su administrador"})
    }
})

router.post('/updateUser/:id', async (req, res) => {
    try{
        let passEncrypt;
        bcrypt.hash(req.body.password, saltRound, (err, hashedPassword) =>{
            if(!err){
                passEncrypt = hashedPassword;
            }
        })

        const db =  await connect();   
        const { id } = req.params;
        
        const updateUser = {
            nombre: req.body.nombre,
            aPaterno: req.body.aPaterno,
            aMaterno: req.body.aMaterno,
            telefono: req.body.telefono,
            direccion: req.body.direccion,
            password: passEncrypt,
            logged: req.body.logged,
            username: req.body.username
        }

        const result = await db.collection('users').updateOne({_id: ObjectID(id)}, {$set: updateUser})

        res.json(updateUser)
        
    } catch(e) {
        res.status(400).json({ "error":"Oucrrío un Problema, consulte con su administrador"})
    }
})

router.post('/loginUser', async (req, res) => {
    console.log(req.body.password)
    try{
        const db =  await connect();   
        let updateUser = {
            password: req.body.password,
            username: req.body.username
        }
        const result = await db.collection('users').findOne({username: updateUser.username})
        
        
        if (result !== null){
            var isCorrect;
            bcrypt.compare( updateUser.password,result.password, async (err, match)=>{
                isCorrect = match
                if(isCorrect){
                    await db.collection('users').updateOne({username: updateUser.username}, {$set: {logged:true}})
                    let response = {
                        nombre: result.nombre,
                        aPaterno: result.aPaterno,
                        aMaterno: result.aMaterno,
                        telefono: result.telefono,
                        direccion: result.direccion,
                        username: result.username,
                        logged: true,
                        id: result._id
                    }
                    res.json(response)
                   } else {
                    res.status(400).json({ "error":"Password incorrecto"})
                   }
            })
        } else {
            res.status(400).json({ "error":"Usuario no encontrado"})
        }
    } catch(e) {
        res.status(400).json({ "error":"Oucrrío un Problema, consulte con su administrador"})
    }
})

router.post('/logout', async (req, res) => {
    try{
        const db =  await connect();   
        const updateUser = {
            username: req.body.username
        }
        const result = await db.collection('users').findOne({username: updateUser.username})
        
        
        if (result !== null){
            await db.collection('users').updateOne({username: updateUser.username}, {$set: {logged:false}})
            res.json({ message: `Usuario ${updateUser.username} Deslogado`})
        } else {
            res.json({ "error":"Usuario no encontrado"})
        }
    } catch(e) {
        res.status(400).json({ "error":"Oucrrío un Problema, consulte con su administrador"})
    }
})

export default router;

