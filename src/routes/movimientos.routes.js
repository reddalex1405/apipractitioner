import {Router} from 'express'
const router =  Router();

//Databases
import {connect} from '../database'
import { ObjectID } from 'mongodb'

router.get('/', async (req, res)=>{
    const db =  await connect();
    const result = await db.collection('movimientos').find({}).toArray();
    res.json(result);
});

router.post('/', async (req, res)=>{
    try{
        const db = await connect();

        const movimiento = {
            concepto: req.body.concepto,
            fecha: new Date(),
            tipopago: req.body.tipopago,
            subtotal: req.body.subtotal,
            total: req.body.total
        }

        const result = await db.collection('movimientos').insertOne(movimiento);
        res.json(result.ops[0])

    } catch(e) {
        res.status(400).json({ "error":"Oucrrío un Problema en el guardado, consulte con su administrador"})
    }
});

export default router;
