import express, {json} from 'express'

const app = express();

//  Settings
app.set('port', process.env.PORT || 3000)

// Middelwares
app.use(json())

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');

    // authorized headers for preflight requests
    // https://developer.mozilla.org/en-US/docs/Glossary/preflight_request
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();

    app.options('*', (req, res) => {
        // allowed XHR methods  
        res.header('Access-Control-Allow-Methods', '*');
        res.send();
    });
});

// Routes
import IndexRoutes  from './routes/index.routes'
import UserRoutes  from './routes/user.routes'
import MovimientosRoutes from './routes/movimientos.routes'

app.use(IndexRoutes); 
app.use('/users', UserRoutes);
app.use('/movimientos', MovimientosRoutes);



export default app;
